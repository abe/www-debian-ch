<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
    <head>
	<title>Matériel promotionnel debian.ch</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="imagetoolbar" content="no" />
	<meta name="author" content="debian.ch" />
	<meta name="description" content="Page du matériel promotionnel debian.ch" />
	<meta name="translator" content="mailto:didier@raboud.com" />
	<link rev="made" href="mailto:info@debian.ch" />
	<link rel="stylesheet" href="../style.css" type="text/css" />
        <link rel="shortcut icon" href="../img/favicon.ico" />
    </head>
    <body>
	<p id="langselect">[
	<a href="index.html.de">deutsch</a> |
	<a href="index.html.en">english</a> |
	<a href="index.html.it">italiano</a>
	]</p>
	<hr id="postlang" />

	<h1>Matériel promotionnel debian.ch</h1>

	<p>debian.ch produit différents articles destinés à être vendus aux
	expositions/rencontres en Suisse.<br />
	Si vous êtes en charge d'un Groupe d'Utilisateurs Linux et que vous
        voudriez revendre ces articles, contactez nous pour convenir d'une
        solution !</p>

        <p>Il est également possible d'acquérir ces articles par voie postale :
        transférez la somme exacte (incluant prix des articles, frais d'envois,
        dons éventuels…) sur notre compte (dont les détails sont accessibles
        sur la <a href="../">page d'accueil</a>) et <a
        href="mailto:sales&#37;&#52;&#48;debian&#46;ch">envoyez-nous un
        courriel</a> quelques jours plus tard</p>

        <p>Conformément à nos <a href="../articles_of_association.pdf">statuts (en
        anglais)</a>, tout l'argent que possède debian.ch est activement
        utilisé pour soutenir le projet Debian sous diverses formes, par
        exemple en participant à des évènements ou en soutenant financièrement
        des <a href="http://wiki.debian.org/BSP">fêtes de chasse aux bugs
        (BSP)</a>.</p>

	<p>Si vous avez d'autres photos des articles ci-dessous, envoyez-nous un
	courriel à <a
	href="mailto:sales&#37;&#52;&#48;debian&#46;ch">sales<span>&#64;</span>debian<span>&#46;</span>ch</a>
	et nous les mettrons ici !</p>
        
	<p>Jetez également un coup d'œil à la <a
        href="http://wiki.debian.org/Merchandise">page wiki générale des
        articles promotionnels Debian</a>.</p>

        <div id="tshirts-whole">
	<h2>T-Shirts</h2>

        <p>Les frais d'envoi par T-shirt s'élèvent à 3 CHF pour les envois en
        Suisse et à 8 CHF pour les envois en Europe.</p>

	<table id="tshirts">
	    <thead>
		<tr><td>Dessin</td><td>Prix</td><td>Photos</td><td>Stock</td></tr>
	    </thead>
	    <tbody>
		<tr> <!-- added: 2009.02.23 gismo -->
	            <td><a
                        href="img/tshirts/debian.ch_classic.png"><img
                        src="img/tshirts/debian.ch_classic.png"
                        alt="Dessin debian.ch classique" /> debian.ch
		        classique</a></td>
		    <td>25 CHF</td>
		    <td><a href="img/tshirts/debian.ch_classic_Akira.jpg">1</a>
			<a href="img/tshirts/debian.ch_classic_OdyX.jpg">2</a>
			<a href="img/tshirts/debian.ch_back_Lixette-Akira-OdyX.jpg">3</a></td>
                    <td id="T_classic" class="stock">!no-js!</td>
		</tr>
	    </tbody>
	</table>

	<p>Merci beaucoup à Lixette, Akira et OdyX de <a
	href="http://swisslinux.org">Swisslinux.org</a> pour les photos des
	T-shirts debian.ch !</p>

        </div>

	<h2>Autocollants</h2>

	<p>Ces autocollants sont faits d'une feuille de vinyl coupé directement
	à la forme du <em>swirl</em> Debian. Ils n'ont donc pas de fond blanc
	ni aucun fond d'ailleurs.</p>

        <p>Les frais d'envoi par autocollant s'élèvent à 1.50 CHF pour les envois en
        Suisse et à 3 CHF pour les envois en Europe.</p>

	<table id="foilstickers">
	    <thead>
		<tr><td>Dessin</td><td>Taille</td><td>Prix</td><td>Photos</td><td>Stock</td></tr>
	    </thead>
	    <tbody>
		<!-- added: 2009.02.23 gismo -->
		<tr>
		    <td rowspan="2"><a
		        href="img/foilstickers/Debian_Women_swirl.png"><img
		        src="img/foilstickers/Debian_Women_swirl.png"
		        alt="Dessin swirl Debian Women" /> Swirl
		        <em>Debian Women</em></a></td>
                    <td>30 x 42 mm</td>
		    <td>2 CHF</td>
		    <td><!-- <a href="img/foilstickers/MISSING.jpg">1</a> --></td>
                    <td id="S_women_XS" class="stock">!no-js!</td>
		</tr>
		<tr>
                    <td>31 x 45 mm</td>
		    <td>2 CHF</td>
		    <td><!-- <a href="img/foilstickers/MISSING.jpg">1</a> --></td>
                    <td id="S_women_S" class="stock">!no-js!</td>
		</tr>
		<!-- added: 2009.02.23 gismo -->
		<tr>
		    <td rowspan="1"><a
		        href="img/foilstickers/Debian_word.png"><img
		        src="img/foilstickers/Debian_word.png"
		        alt="Dessin mot debian, noir avec le point sur
		        le i rouge" /> Le mot "debian"<br /> (noir ou
		        blanc, avec le<br /> point sur le <em>i</em>
		        rouge)</a></td>
		    <td>36.3 x 11.8 mm</td>
		    <td>2 CHF</td>
		    <td><!-- <a href="img/foilstickers/MISSING.jpg">1</a> --></td>
                    <td class="stock">Noir: <span id="S_word_black_S">!no-js!</span><br/>
                                      Blanc: <span id="S_word_white_S">!no-js!</span></td>
		</tr>
                <!-- added: 2011.01.02 gismo -->
                <tr>
                    <td rowspan="2"><a href="img/foilstickers/Debian_kFreeBSD_swirl.png"><img
                        src="img/foilstickers/Debian_kFreeBSD_swirl.png"
                        alt="Logo Debian avec les cornes BSD" align="left"
                        /> Logo Debian avec les cornes BSD<br />(logo Debian GNU/kFreeBSD)</a></td>
                    <td>30 x 32 mm</td>
                    <td>2 CHF</td>
                    <td><!-- <a href="img/foilstickers/Debian_swirl_Openmoko-FreeRunner.jpg">1</a> --></td>
                    <td id="S_horned_XS" class="stock">!no-js!</td>
                </tr>
                <tr>
                    <td>38 x 40 mm</td>
                    <td>2 CHF</td>
                    <td><!-- <a href="img/foilstickers/Debian_swirl_Openmoko-FreeRunner.jpg">1</a> --></td>
                    <td id="S_horned_S" class="stock">!no-js!</td>
                </tr>
	    </tbody>
	</table>

        <div id="umbrellas-whole">
        <h2>Parapluies</h2>

        <p>Il s'agit de parapluies pliables, donc relativement petits lorsque
        pliés.</p>

        <p>Étant donné que les frais d'envois pour un seul parapluie sont
        démesurés par rapport à la valeur du parapluie, il est recommandé de
        commander au moins <big>trois parapluies</big>. Les frais d'envoi pour
        trois parapluies se montent à 7 CHF pour la Suisse, 12.50 CHF pour
        l'Europe et 18 CHF pour le reste du monde (le prix est doublé pour
        jusqu'à 6 parapluies). Un parapluie pèse environ 220 g, et vous pouvez
        choisir votre mode d'envoi sur le <a
	href="http://www.post.ch/fr/post-startseite/post-privatkunden/post-versenden/post-versenden-ausland-paket.htm">site
        de la Poste Suisse</a>. Merci de confirmer votre choix dans votre
        courriel à <a
        href="mailto:sales&#37;&#52;&#48;debian&#46;ch">sales<span>&#64;</span>debian<span>&#46;</span>ch</a>.</p>

        <table id="umbrellas">
            <thead>
                <tr><td>Dessin</td><td>Prix</td><td>Photos</td><td>Stock</td></tr>
            </thead>
            <tbody>
                <tr> <!-- added: 2011.01.02 gismo -->
                    <td><a

href="img/umbrellas/Debian_openlogo-100_alpha.png"><img
                        src="img/umbrellas/Debian_openlogo-100_alpha.png"
                        alt="Openlogo Debian sur fond blanc ou noir" />
                        Openlogo Debian sur fond blanc ou noir</a></td>
                    <td>25 CHF</td>
                    <td><a href="img/umbrellas/debianumbrella1.jpg">1</a>
                        <a href="img/umbrellas/debianumbrella2.jpg">2</a>
                        <a href="img/umbrellas/Debian_umbrella-black.640.jpg">3</a></td>
                    <td class="stock">
                        Noir: <span id="U_black">!no-js!</span><br />
                        Blanc: <span id="U_white">!no-js!</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- Load the javascript at the very end -->
    <script language="javascript" type="text/javascript" src="stock.js"></script>

    </body>
</html>

<!-- vim: set sw=4 et: -->
