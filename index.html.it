<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="it" xml:lang="it">
    <head>
        <title>debian.ch</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="author" content="debian.ch" />
        <meta name="description" content="homepage dell'associazione debian.ch" />
        <meta name="translator" content="mailto:luca&#37;&#52;&#48;pca&#46;it" />
        <link rev="made" href="mailto:info&#37;&#52;&#48;debian&#46;ch" />
        <link rel="stylesheet" href="style.css" type="text/css" />
        <link rel="shortcut icon" href="img/favicon.ico" />
    </head>
    <body>
        <p id="langselect">[
        <a href="index.html.de">deutsch</a> |
        <a href="index.html.fr">français</a> |
        <a href="index.html.en">english</a>
        ]</p>
        <hr id="postlang" />

        <h1><img src="img/debian_ch-text.png" width="319" height="75"
                alt="debian.ch" /></h1>

        <p>L'associazione debian.ch rappresenta il progetto <a
          href="https://www.debian.org/">Debian</a> per la confederazione
        Svizzera e il principato del Liechtenstein.  È un'organizzazione
        non-profit secondo la legge svizzera (<a
          href="articles_of_association.pdf">statuto, in inglese</a>).</p>

        <p>debian.ch è raggiungibile via:</p>

        <ul>
            <li>E-Mail: <a href="mailto:info&#37;&#52;&#48;debian&#46;ch">info<span>&#64;</span>debian<span>&#46;</span>ch</a></li>
            <li>Posta:<br/> <address>debian.ch<br/>3000 Berna</address></li>
            <li>Conto per donazioni all'interno della Svizzera:
              conto PostFinance <em>85-361360-7</em>, beneficiario
              <em>debian.ch</em>.<br/>
              Vi preghiamo di tener presente che ogni deposito di
              denaro liquido ha un prezzo fisso di 1.80 CHF.</li>
            <li><a
              href="https://www.postfinance.ch/pf/content/it/seg/biz/product/pay/internat/inbound.html"
              target="_blank">Bonifici dall'estero</a> devono essere
              intestati al conto <em>debian.ch</em> (IBAN <em>CH12
              0900 0000 8536 1360 7</em>) presso la banca PostFinance
              (BIC/SWIFT <em>POFICHBEXXX</em>).<br/>
              L'indirizzo del beneficiario è lo stesso indirizzo
              postale riportato prima.<br/>
	      L'indirizzo della banca è:<br/>
	      <address>La Posta Svizzera - PostFinance<br/>
		Nordring 8<br/>
		3030 Berna<br/>
		Svizzera</address></li>

            <li>Domande riguardo le donazioni: <a
              href="mailto:donate&#37;&#52;&#48;debian&#46;ch">donate<span>&#64;</span>debian<span>&#46;</span>ch</a></li>
        </ul>

        <h2>Comunità / Meetings</h2>

        <p>Le persone interessate alle attività riguardanti Debian in
        Svizzera si ritrovano sulla mailing list <a
         href="https://lists.debian.org/debian-switzerland/">debian-switzerland<span>&#64;</span>lists<span>&#46;</span>debian<span>&#46;</span>org</a>.</p>

        <h2>Materiale Promozionale</h2>

        <p>A nome del progetto Debian, debian.ch produce e vende <a
            href="merchandise/">materiale promozionale Debian</a>.  Il
        ricavato, senza alcuna deduzione, viene trasferito al progetto
        Debian.</p>

        <h2>Supporto</h2>

        <p>A causa delle risorse limitate, <b>debian.ch non fornisce supporto per
          Debian GNU/Linux</b>. <a href="https://www.debian.org/support">Il
          sito web di Debian</a> ha una pagina che elenca vari modi per ottenere
        supporto (sia commerciale che gratuito).</p>
    </body>
</html>

<!-- vim: set sw=4 et: -->
